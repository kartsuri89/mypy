import yfinance as yf

# Download historical data for a stock
data = yf.download('AAPL', start='2020-01-01', end='2022-01-01')

data.to_csv('stock_data.csv')