import sys
print("Hai :::: " + sys.version)
import numpy as np
import pandas as pd
from sklearn.preprocessing import MinMaxScaler
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import LSTM, Dense
#from keras.models import Sequential
#from keras.layers import LSTM, Dense
#import matplotlib
#matplotlib.use('Agg')
import matplotlib.pyplot as plt

# Load the dataset
data = pd.read_csv('stock_data.csv')

# Preprocess the data
scaler = MinMaxScaler(feature_range=(0, 1))
scaled_data = scaler.fit_transform(data['Close'].values.reshape(-1, 1))

# Create training and testing datasets
train_size = int(len(scaled_data) * 0.8)
train_data = scaled_data[:train_size]
test_data = scaled_data[train_size:]


def create_dataset(dataset, time_steps=1):
    X, Y = [], []
    for i in range(len(dataset) - time_steps - 1):
        a = dataset[i:(i + time_steps), 0]
        X.append(a)
        Y.append(dataset[i + time_steps, 0])
    return np.array(X), np.array(Y)


time_steps = 30
X_train, Y_train = create_dataset(train_data, time_steps)
X_test, Y_test = create_dataset(test_data, time_steps)

# Reshape input to be [samples, time steps, features]
X_train = np.reshape(X_train, (X_train.shape[0], X_train.shape[1], 1))
X_test = np.reshape(X_test, (X_test.shape[0], X_test.shape[1], 1))

# Build the RNN model
model = Sequential()
model.add(LSTM(units=50, return_sequences=True, input_shape=(X_train.shape[1], 1)))
model.add(LSTM(units=50, return_sequences=False))
model.add(Dense(units=1))

# Compile the model
model.compile(optimizer='adam', loss='mean_squared_error')

# Train the model
model.fit(X_train, Y_train, epochs=100, batch_size=32)

# Make predictions
predictions = model.predict(X_test)
predictions = scaler.inverse_transform(predictions)
# Plot the results

plt.figure(figsize=(10, 6))
plt.plot(range(train_size + time_steps, len(data)), data['Close'].values[train_size + time_steps:], label='Actual Stock Price')
#plt.plot(range(train_size + time_steps, len(data)), predictions, label='Predicted Stock Price')
plt.plot(range(train_size + time_steps, train_size + time_steps + len(predictions)), predictions, label='Predicted Stock Price')

plt.xlabel('Time')
plt.ylabel('Stock Price')
plt.title('Stock Price Prediction Using RNN')
plt.legend()
plt.show()

